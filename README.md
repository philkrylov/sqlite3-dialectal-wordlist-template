## sqlite3-dialectal-wordlist-template

An SQLite3 template for a dialectal wordlist database.

The design is aimed to be compatible with [CLDF](https://cldf.clld.org) but
also includes [Concepticon](https://concepticon.clld.org) data to relate your
concepts with, [Glottolog](https://glottolog.org),
[ISO-639-3](https://iso639-3.sil.org/code_tables/639/data),
and [ISO-639-5](https://www.loc.gov/standards/iso639-5/id.php) data
for language metadata.

The binaries in `sqliteext` folder are taken from
[sqlite-http](https://github.com/asg017/sqlite-http/releases/tag/v0.1.1).

Usage:

    sqlite3 my-database-name.db < template/dialectal_wordlist.sql
