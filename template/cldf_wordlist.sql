.echo on
.bail on

-- Load required extensions
.load sqliteext/http0
-- .load sqliteext/json1

-- Clean up
DROP TABLE IF EXISTS iso639p3;
DROP TABLE IF EXISTS iso639p5;
DROP TABLE IF EXISTS language;
DROP TABLE IF EXISTS macroarea;
DROP TABLE IF EXISTS glottolog;
DROP TABLE IF EXISTS FormTable;
DROP TABLE IF EXISTS LanguageTable;
DROP TABLE IF EXISTS ParameterTable;

-- Macro-area: https://glottolog.org/glossary#macroarea
CREATE TABLE macroarea (
  id TEXT NOT NULL PRIMARY KEY
); -- STRICT is not supported everywhere
-- populate macro-areas
INSERT INTO macroarea (id) VALUES ('Africa'), ('Australia'), ('Eurasia'), ('North America'), ('Papunesia'), ('South America');

-- ISO 639-3 languages. Immutable table
CREATE TABLE iso639p3 (
    id       TEXT NOT NULL PRIMARY KEY,  -- The three-letter 639-3 identifier
    part2b   TEXT NULL,                  -- Equivalent 639-2 identifier of the bibliographic applications code set, if there is one
    part2t   TEXT NULL,                  -- Equivalent 639-2 identifier of the terminology applications code set, if there is one
    part1    TEXT NULL,                  -- Equivalent 639-1 identifier, if there is one
    scope    TEXT NOT NULL,              -- I(ndividual), M(acrolanguage), S(pecial)
    language_type TEXT NOT NULL,         -- A(ncient), C(onstructed), E(xtinct), H(istorical), L(iving), S(pecial)
    ref_name TEXT NOT NULL,              -- Reference language name
    "comment" TEXT NULL                  -- Comment relating to one or more of the columns
); -- STRICT is not supported everywhere
CREATE INDEX ix_iso639p3_ref_name ON iso639p3 (ref_name COLLATE NOCASE);
-- populate ISO 639-3 table
SELECT writefile('iso-639-3.tab.tmp', http_get_body('https://iso639-3.sil.org/sites/iso639-3/files/downloads/iso-639-3.tab'));
.mode tabs
.import -v --skip 1 iso-639-3.tab.tmp iso639p3


-- ISO 639-5 language groups. Immutable table
CREATE TABLE iso639p5 (
  id TEXT NOT NULL PRIMARY KEY,
  labelen TEXT NOT NULL,
  labelfr TEXT
); -- STRICT is not supported everywhere
CREATE INDEX ix_iso639p5_labelen ON iso639p5 (labelen COLLATE NOCASE);
CREATE INDEX ix_iso639p5_labelfr ON iso639p5 (labelfr COLLATE NOCASE);
-- populate ISO 639-5 table
SELECT writefile('iso-639-5.tsv.tmp', http_get_body('https://id.loc.gov/vocabulary/iso639-5.tsv'));
.mode tabs
.import -v iso-639-5.tsv.tmp temp_isotab
INSERT INTO iso639p5 SELECT code, "Label (English)", "Label (French)" FROM temp_isotab;
DROP TABLE temp_isotab;

-- Glottolog. Immutable table
CREATE TABLE glottolog (
  id TEXT NOT NULL PRIMARY KEY,
  name TEXT NOT NULL,
  macroarea TEXT,
  latitude REAL,
  longitude REAL,
  glottocode TEXT NOT NULL,
  iso639p3code TEXT,
  countries TEXT,
  family_id TEXT,
  language_id TEXT,
  closest_iso369p3code TEXT,
  first_year_of_documentation INTEGER,
  last_year_of_documentation INTEGER,
  FOREIGN KEY (macroarea) REFERENCES macroarea (id),
  FOREIGN KEY (iso639p3code) REFERENCES iso639p3 (id)
); -- STRICT is not supported everywhere
-- populate Glottolog table
SELECT writefile('languages.csv.tmp', http_get_body('https://raw.githubusercontent.com/glottolog/glottolog-cldf/master/cldf/languages.csv'));
.mode csv
DROP TABLE IF EXISTS temp_glottolog;
.import -v languages.csv.tmp temp_glottolog
INSERT INTO glottolog
    SELECT
        Id,
        Name,
        nullif(Macroarea,''),
        cast(nullif(Latitude, '') AS REAL),
        cast(nullif(Longitude, '') AS REAL),
        Glottocode,
        nullif(ISO639P3code,''),
        Countries,
        nullif(Family_ID,''),
        nullif(Language_ID,''),
        nullif(Closest_ISO369P3code,''),
        cast(nullif(First_Year_Of_Documentation, '') AS INTEGER),
        cast(nullif(Last_Year_Of_Documentation, '') AS INTEGER)
        FROM temp_glottolog;
DROP TABLE temp_glottolog;

-- Language metadata as per CLDF spec v1.2, for project-specific languages
CREATE TABLE LanguageTable (
    cldf_id TEXT NOT NULL PRIMARY KEY,
    cldf_name TEXT NOT NULL,
    cldf_macroarea TEXT NOT NULL,
    cldf_latitude REAL,
    cldf_longitude REAL,
    cldf_glottocode TEXT,
    cldf_iso639P3code TEXT,
    FOREIGN KEY (cldf_macroarea) REFERENCES macroarea (id),
    FOREIGN KEY (cldf_glottocode) REFERENCES glottocode (id),
    FOREIGN KEY (cldf_iso639P3code) REFERENCES iso639p3 (id)
); -- STRICT is not supported everywhere
CREATE INDEX ix_languagetable_name ON LanguageTable (cldf_name COLLATE NOCASE);

-- Parameter metadata as per CLDF spec v1.2
CREATE TABLE ParameterTable (
    cldf_id TEXT NOT NULL PRIMARY KEY,
    cldf_name TEXT NOT NULL,
    cldf_description TEXT,
    cldf_columnSpec JSON
);
CREATE INDEX ix_parametertable_name ON ParameterTable (cldf_name COLLATE NOCASE);

-- Form data as per CLDF spec v1.2
CREATE TABLE FormTable (
    cldf_id INTEGER NOT NULL PRIMARY KEY,
    cldf_languageReference TEXT NOT NULL,
    cldf_parameterReference TEXT NOT NULL,
    cldf_form TEXT NOT NULL,
    cldf_segments TEXT,
    cldf_comment TEXT,
    cldf_source TEXT,
    FOREIGN KEY (cldf_languageReference) REFERENCES LanguageTable (cldf_id),
    FOREIGN KEY (cldf_parameterReference) REFERENCES ParameterTable (cldf_id)
); -- STRICT is not supported everywhere
CREATE INDEX ix_formtable_form ON FormTable (cldf_form COLLATE NOCASE);
