-- Usage: sqlite3 filename.db < template/dialectal_wordlist.sql
.echo on
.bail on
-- Apply base templates
.read template/cldf_wordlist.sql
.read template/concepticon.sql

-- Clean up
DROP TABLE IF EXISTS diagnostic;
DROP TABLE IF EXISTS languagegroup;

-- Project-specific language groups
CREATE TABLE languagegroup (
  id TEXT NOT NULL PRIMARY KEY,
  isocode TEXT,
  labelen TEXT NOT NULL,
  labelru TEXT,
  FOREIGN KEY (isocode) REFERENCES iso639p5 (id)
); -- STRICT is not supported everywhere
CREATE INDEX ix_languagegroup_labelen ON languagegroup (labelen COLLATE NOCASE);
CREATE INDEX ix_languagegroup_labelru ON languagegroup (labelru COLLATE NOCASE);

-- Language metadata for project-specific languages, extra columns
ALTER TABLE LanguageTable ADD COLUMN fixation_century INTEGER DEFAULT (20);
ALTER TABLE LanguageTable ADD COLUMN location TEXT;
ALTER TABLE LanguageTable ADD COLUMN informant TEXT;
ALTER TABLE LanguageTable ADD COLUMN languagegroup TEXT REFERENCES languagegroup (id);

CREATE TABLE diagnostic (
    id INTEGER NOT NULL PRIMARY KEY,
    text TEXT NOT NULL UNIQUE
);

-- Concept metadata, as per CLDF spec v1.2
DROP TABLE ParameterTable; -- Easier to drop than to alter
CREATE TABLE ParameterTable (
    cldf_id TEXT NOT NULL PRIMARY KEY,
    cldf_name TEXT NOT NULL,
    cldf_description TEXT,
    cldf_columnSpec JSON,
    gld_id INTEGER NOT NULL,
    concepticon_id INTEGER,
    meaningsru TEXT,
    FOREIGN KEY (concepticon_id) REFERENCES concepticon (id)
);
CREATE INDEX ix_parametertable_name ON ParameterTable (cldf_name COLLATE NOCASE);
CREATE INDEX ix_parametertable_gld_id ON ParameterTable (gld_id COLLATE NOCASE);

-- Form data as per CLDF spec v1.2
DROP TABLE FormTable; -- Easier to drop than to alter
CREATE TABLE FormTable (
    cldf_id INTEGER NOT NULL PRIMARY KEY,
    cldf_languageReference TEXT NOT NULL,
    cldf_parameterReference TEXT NOT NULL,
    cldf_form TEXT NOT NULL,
    cldf_segments TEXT,
    cldf_comment TEXT,
    cldf_source TEXT,
    diagnostic_id INTEGER,
    etymology_id INTEGER CHECK (etymology_id > 0),
    loanword_id INTEGER CHECK (loanword_id < 0),
    etymology TEXT,
    derivation TEXT,
    semantics TEXT,
    context TEXT,
    FOREIGN KEY (cldf_languageReference) REFERENCES LanguageTable (cldf_id),
    FOREIGN KEY (cldf_parameterReference) REFERENCES ParameterTable (cldf_id),
    FOREIGN KEY (diagnostic_id) REFERENCES diagnostic (id),
    FOREIGN KEY (etymology_id) REFERENCES etymology (id)
); -- STRICT is not supported everywhere
CREATE INDEX ix_formtable_form ON FormTable (cldf_form COLLATE NOCASE);
