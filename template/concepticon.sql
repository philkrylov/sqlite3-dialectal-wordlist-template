.echo on
.bail on
-- Load required extensions
.load sqliteext/http0

-- Clean up
DROP TABLE IF EXISTS concepticon;

-- Concepticon. Immutable table
CREATE TABLE concepticon (
    id INTEGER NOT NULL PRIMARY KEY,
    gloss TEXT NOT NULL,
    semanticfield TEXT NOT NULL,
    definition TEXT NOT NULL,
    ontological_category TEXT NOT NULL,
    replacement_id INTEGER,
    FOREIGN KEY (replacement_id) REFERENCES concepticon (id)
); -- STRICT is not supported everywhere
CREATE INDEX ix_concepticon_gloss ON concepticon (gloss COLLATE NOCASE);
CREATE INDEX ix_concepticon_semanticfield ON concepticon (semanticfield COLLATE NOCASE);
CREATE INDEX ix_concepticon_ontological_category ON concepticon (ontological_category COLLATE NOCASE);

-- populate Concepticon table
SELECT writefile('concepticon.tsv.tmp', http_get_body('https://github.com/concepticon/concepticon-data/raw/master/concepticondata/concepticon.tsv'));
DROP TABLE IF EXISTS temp_concepticon;
.mode tabs
.import concepticon.tsv.tmp temp_concepticon
INSERT INTO concepticon
    SELECT
        CAST(id AS INTEGER),
        gloss,
        semanticfield,
        definition,
        ontological_category,
        CAST(nullif(replacement_id, '') AS INTEGER)
    FROM temp_concepticon;
DROP TABLE temp_concepticon;
